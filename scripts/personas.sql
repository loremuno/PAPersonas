-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generaciÃ³n: 13-11-2017 a las 21:24:22
-- VersiÃ³n del servidor: 10.1.28-MariaDB
-- VersiÃ³n de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `personas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `telefonoFijoContacto` varchar(100) NOT NULL,
  `celularContacto` varchar(100) NOT NULL,
  `telefonoAlternativoContacto` varchar(100) NOT NULL,
  `correoElectronicoContacto` varchar(100) NOT NULL,
  `OIDContacto` int(11) NOT NULL,
  `bajaContacto` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`telefonoFijoContacto`, `celularContacto`, `telefonoAlternativoContacto`, `correoElectronicoContacto`, `OIDContacto`, `bajaContacto`) VALUES
('1', '1', '1', '1', 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `OIDDomicilio` int(11) NOT NULL,
  `calleDomicilio` varchar(50) NOT NULL,
  `numeroDomicilio` varchar(10) NOT NULL,
  `pisoDomicilio` varchar(10) NOT NULL,
  `deptoDomicilio` varchar(10) NOT NULL,
  `OIDLocalidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`OIDDomicilio`, `calleDomicilio`, `numeroDomicilio`, `pisoDomicilio`, `deptoDomicilio`, `OIDLocalidad`) VALUES
(1, 'Ruta 20', '9475', '', '', 1),
(2, '1', '1', '1', '1', 1),
(3, '2', '2', '2', '2', 1),
(4, '2', '22', '2', '2', 1),
(7, '2', '2', '211', '1', 1),
(8, 'barrio santa rita', '17', '-', '-', 1),
(52, '1asdasd', '1', '1', '1', 1),
(53, '5', '5', '5', '5', 1),
(54, '5123asd', '5', '5', '5', 1),
(55, '21', '21', '1', '21', 1),
(58, '1', '151', '1', '51', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocivil`
--

CREATE TABLE `estadocivil` (
  `OIDEstadoCivil` int(11) NOT NULL,
  `nombreEstadoCivil` varchar(50) NOT NULL,
  `bajaEstadoCivil` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estadocivil`
--

INSERT INTO `estadocivil` (`OIDEstadoCivil`, `nombreEstadoCivil`, `bajaEstadoCivil`) VALUES
(1, 'Soltero', 0),
(2, 'Conyugue', 0),
(3, 'Casado', 0),
(4, 'Viudo', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `OIDGenero` int(11) NOT NULL,
  `nombreGenero` varchar(50) NOT NULL,
  `bajaGenero` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`OIDGenero`, `nombreGenero`, `bajaGenero`) VALUES
(1, 'Masculino', 0),
(2, 'Femenino', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `OIDLocalidad` int(11) NOT NULL,
  `nombreLocalidad` varchar(50) NOT NULL,
  `bajaLocalidad` tinyint(1) NOT NULL,
  `OIDProvincia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `localidad`
--

INSERT INTO `localidad` (`OIDLocalidad`, `nombreLocalidad`, `bajaLocalidad`, `OIDProvincia`) VALUES
(1, 'Buena Nueva', 0, 1),
(2, 'Carlos Paz', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `OIDPais` int(11) NOT NULL,
  `nombrePais` varchar(50) NOT NULL,
  `bajaPais` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`OIDPais`, `nombrePais`, `bajaPais`) VALUES
(1, 'Argentina', 0),
(2, 'Chile', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `OIDPersona` int(11) NOT NULL,
  `dniPersona` varchar(10) NOT NULL,
  `cuilPersona` varchar(20) NOT NULL,
  `nombrePersona` varchar(50) NOT NULL,
  `apellidoPersona` varchar(50) NOT NULL,
  `OIDDomicilio` int(11) NOT NULL,
  `OIDGenero` int(11) NOT NULL,
  `OIDEstadoCivil` int(11) NOT NULL,
  `bajaPersona` tinyint(1) NOT NULL,
  `fechaNacimientoPersona` date NOT NULL,
  `apellidoMaternoPersona` varchar(50) NOT NULL,
  `OIDContacto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`OIDPersona`, `dniPersona`, `cuilPersona`, `nombrePersona`, `apellidoPersona`, `OIDDomicilio`, `OIDGenero`, `OIDEstadoCivil`, `bajaPersona`, `fechaNacimientoPersona`, `apellidoMaternoPersona`, `OIDContacto`) VALUES
(54, '1', '1', '1', '1', 58, 1, 1, 0, '0001-01-01', '1', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `OIDProvincia` int(11) NOT NULL,
  `nombreProvincia` varchar(50) NOT NULL,
  `bajaProvincia` tinyint(1) NOT NULL,
  `OIDPais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`OIDProvincia`, `nombreProvincia`, `bajaProvincia`, `OIDPais`) VALUES
(1, 'Mendoza', 0, 1),
(2, 'Cordoba', 0, 1);

--
-- Ãndices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`OIDContacto`),
  ADD UNIQUE KEY `unicaContacto` (`telefonoFijoContacto`,`celularContacto`,`telefonoAlternativoContacto`,`correoElectronicoContacto`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`OIDDomicilio`),
  ADD UNIQUE KEY `unicaDomicilio` (`numeroDomicilio`,`calleDomicilio`,`pisoDomicilio`,`deptoDomicilio`),
  ADD KEY `relacionDomicilioLocalidad` (`OIDLocalidad`);

--
-- Indices de la tabla `estadocivil`
--
ALTER TABLE `estadocivil`
  ADD PRIMARY KEY (`OIDEstadoCivil`),
  ADD UNIQUE KEY `nombreEstadoCivil` (`nombreEstadoCivil`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`OIDGenero`),
  ADD UNIQUE KEY `nombreGenero` (`nombreGenero`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD PRIMARY KEY (`OIDLocalidad`),
  ADD UNIQUE KEY `OIDLocalidad` (`OIDLocalidad`),
  ADD KEY `relacionLocalidadProvincia` (`OIDProvincia`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`OIDPais`),
  ADD UNIQUE KEY `nombrePais` (`nombrePais`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`OIDPersona`),
  ADD UNIQUE KEY `unicaPersona` (`cuilPersona`),
  ADD KEY `relacionPersonaDomicilio` (`OIDDomicilio`),
  ADD KEY `relacionPersonaGenero` (`OIDGenero`),
  ADD KEY `relacionPersonaEstadoCivil` (`OIDEstadoCivil`),
  ADD KEY `relacionPersonaContacto` (`OIDContacto`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`OIDProvincia`),
  ADD UNIQUE KEY `OIDProvincia` (`OIDProvincia`),
  ADD KEY `relacionProvinciaPais` (`OIDPais`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `OIDContacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `OIDDomicilio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `estadocivil`
--
ALTER TABLE `estadocivil`
  MODIFY `OIDEstadoCivil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `OIDGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `localidad`
--
ALTER TABLE `localidad`
  MODIFY `OIDLocalidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `OIDPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `OIDPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `OIDProvincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `relacionDomicilioLocalidad` FOREIGN KEY (`OIDLocalidad`) REFERENCES `localidad` (`OIDLocalidad`);

--
-- Filtros para la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD CONSTRAINT `relacionLocalidadProvincia` FOREIGN KEY (`OIDProvincia`) REFERENCES `provincia` (`OIDProvincia`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `relacionPersonaContacto` FOREIGN KEY (`OIDContacto`) REFERENCES `contacto` (`OIDContacto`),
  ADD CONSTRAINT `relacionPersonaDomicilio` FOREIGN KEY (`OIDDomicilio`) REFERENCES `domicilio` (`OIDDomicilio`),
  ADD CONSTRAINT `relacionPersonaEstadoCivil` FOREIGN KEY (`OIDEstadoCivil`) REFERENCES `estadocivil` (`OIDEstadoCivil`),
  ADD CONSTRAINT `relacionPersonaGenero` FOREIGN KEY (`OIDGenero`) REFERENCES `genero` (`OIDGenero`);

--
-- Filtros para la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `relacionProvinciaPais` FOREIGN KEY (`OIDPais`) REFERENCES `pais` (`OIDPais`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
