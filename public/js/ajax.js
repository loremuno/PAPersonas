$(function () {
        function llenarGenero() {
                var url = "api/accionesGenero.php?habilitado=1";
                $.ajax({
                        data: '',
                        url: url,
                        type: 'get',
                        beforeSend: function () {
                                $("#generoContent").html("Procesando, espere por favor...");
                        },
                        success: function (response) {
                                $("#generoContent").html('');
                                var datos = JSON.parse(response);
                                datos.forEach(function (element) {
                                        $("#generoContent").append(`
                                        <div class="radio form-check">
                                        <label class="form-check-label">
                                                <input required class="form-check-input" type="radio" name="OIDGenero" value="`+ element.OIDGenero + `"> ` + element.nombreGenero + `
                                        </label>
                                        </div>
                                    `);
                                }, this);
                        }
                });
        }
        function llenarEstadoCivil() {
                var url = "api/accionesEstadoCivil.php?habilitado=1";
                $.ajax({
                        data: '',
                        url: url,
                        type: 'get',
                        beforeSend: function () {
                                $("#estadoCivilContent").html("Procesando, espere por favor...");
                        },
                        success: function (response) {
                                $("#estadoCivilContent").html('');
                                var datos = JSON.parse(response);
                                datos.forEach(function (element) {
                                        $("#estadoCivilContent").append(`
                                        <div class="radio form-check">
                                        <label class="form-check-label">
                                                <input required class="form-check-input" type="radio" name="OIDEstadoCivil" value="`+ element.OIDEstadoCivil + `"> ` + element.nombreEstadoCivil + `
                                        </label>
                                        </div>
                                    `);
                                }, this);
                        }
                });
        }
        function llenarPais() {
                var url = "api/accionesPais.php?habilitado=1";
                $.ajax({
                        data: '',
                        url: url,
                        type: 'get',
                        beforeSend: function () {

                        },
                        success: function (response) {
                                var datos = JSON.parse(response);
                                datos.forEach(function (element) {
                                        $("#paisSelect").append('<option value="' + element.OIDPais + '">' + element.nombrePais + '</option>');
                                }, this);
                        }
                });
        }
        function llenarProvincia() {
                var idPais = $("#paisSelect option:selected").val();
                var url = "api/accionesProvincia.php?habilitado=" + idPais;
                $.ajax({
                        data: '',
                        url: url,
                        type: 'get',
                        beforeSend: function () {

                        },
                        success: function (response) {
                                $("#provinciaSelect").html('<option selected disabled value="">Seleccione la opción</option>');
                                var datos = JSON.parse(response);
                                datos.forEach(function (element) {
                                        $("#provinciaSelect").append('<option value="' + element.OIDProvincia + '">' + element.nombreProvincia + '</option>');
                                }, this);

                                $("#contenedorProvincia").toggle(true);
                                $("#contenedorLocalidad").toggle(false);
                        }
                });
        }
        function llenarLocalidad() {
                var idProvincia = $("#provinciaSelect option:selected").val();
                var url = "api/accionesLocalidad.php?habilitado=" + idProvincia;
                $.ajax({
                        data: '',
                        url: url,
                        type: 'get',
                        beforeSend: function () {

                        },
                        success: function (response) {
                                $("#localidadSelect").html('<option selected disabled value="">Seleccione la opción</option>');
                                var datos = JSON.parse(response);
                                datos.forEach(function (element) {
                                        $("#localidadSelect").append('<option value="' + element.OIDLocalidad + '">' + element.nombreLocalidad + '</option>');
                                }, this);
                                $("#contenedorLocalidad").toggle(true);
                        }
                });
        }
        llenarGenero();
        llenarEstadoCivil();
        llenarPais();

        $("#contenedorProvincia").toggle(false);
        $("#contenedorLocalidad").toggle(false);
        $("#error").toggle(false);
        $("#exito").toggle(false);

        $("#paisSelect")
                .change(function () {
                    $("#error").toggle(false);
        $("#exito").toggle(false);
                        llenarProvincia();
                });


        $("#provinciaSelect")
                .change(function () {
                    $("#error").toggle(false);
        $("#exito").toggle(false);
                        llenarLocalidad();
                });

        $('#form').validator().on('submit', function (e) {
            $("#error").html();
            $("#error").toggle(false);


          var data = $('#form').serializeArray().reduce((obj, item) => {
            obj[item.name] = item.value;
            return obj;
          }, {});


            if (e.isDefaultPrevented()) {
                $("#error").html('<strong>Error!</strong>Complete los campos obligatorios');
                $("#error").toggle(true);
            } else {
                console.log('Enviando', data);



              guardarPersona();
              return false;
            }

          return false;
        });

        function guardarPersona() {
            $("#error").toggle(false);
            $("#error").html();
            $("#exito").toggle(false);
            $("#exito").html();
                var persona = {
                        dniPersona: $("#inputDni").val(),
                        nombrePersona: $("#inputNombre").val(),
                        apellidoPersona: $("#inputApellido").val(),
                        fechaNacimientoPersona: $("#inputFechaNacimiento").val(),
                        apellidoMaternoPersona: $("#inputApellidoMaterno").val(),
                        cuilPersona: $("#inputCuil").val(),
                        OIDGenero: $("input[name='OIDGenero']").val(),
                        OIDEstadoCivil: $("input[name='OIDEstadoCivil']").val(),

                        calleDomicilio: $("#inputCalle").val(),
                        numeroDomicilio: $("#inputNumero").val(),
                        pisoDomicilio: $("#inputPiso").val(),
                        deptoDomicilio: $("#inputDpto").val(),
                        OIDLocalidad: $("#localidadSelect option:selected").val(),

                        telefonoFijoContacto: $("#inputTelefonoFijo").val(),
                        celularContacto: $("#inputCelular").val(),
                        telefonoAlternativoContacto: $("#inputTelefonoAlternativo").val(),
                        correoElectronicoContacto: $("#inputCorreo").val()
                }
                persona.fechaNacimientoPersona = persona.fechaNacimientoPersona.replace("-", "");
                persona.fechaNacimientoPersona = persona.fechaNacimientoPersona.replace("-", "");

                var url = "api/personas.php";
                $.ajax({
                        data: JSON.stringify(persona),
                        url: url,
                        type: 'post',
                        beforeSend: function () {

                        },
                        success: function (response) {
                            console.log(response);
                            $("#exito").html('<strong>Éxito!</strong>Persona guardada con éxito');
                            $("#exito").toggle(true);
                        },
                        error: function(xhr, error){
                            $("#error").html('<strong>Error!</strong>'+xhr.responseText);
                            $("#error").toggle(true);
                        },
                });
        }
});
