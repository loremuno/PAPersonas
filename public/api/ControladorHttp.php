<?php

interface ControladorHttp {
  public function getById($id);
  public function get();
  public function post();
  public function put();
  public function delete();
}
