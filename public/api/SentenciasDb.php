<?php

/**
 * Interface donde defino las querys que se utilizarán en la aplicación
 *
 * @author ieltxu
 */
interface SentenciasDb {
  const BUSCAR_PERSONAS = "SELECT * FROM persona p inner join contacto c on c.OIDContacto = p.OIDContacto inner join domicilio d on d.OIDDomicilio = p.OIDDomicilio inner join localidad l on l.OIDLocalidad = d.OIDLocalidad inner join provincia pro on pro.OIDProvincia = l.OIDProvincia inner join pais pa on pa.OIDPais = pro.OIDPais WHERE bajaPersona = 0";
  
  const INSERTAR_CONTACTO = "INSERT INTO contacto (celularContacto, correoElectronicoContacto, telefonoAlternativoContacto,telefonoFijoContacto,bajaContacto)  VALUES (?,?,?,?,0)";
  const INSERTAR_DOMICILIO = "INSERT INTO domicilio (calleDomicilio, numeroDomicilio, pisoDomicilio,deptoDomicilio,OIDLocalidad)  VALUES (?,?,?,?,?)";
  const INSERTAR_PERSONA = "INSERT INTO persona (OIDContacto,apellidoMaternoPersona,fechaNacimientoPersona, dniPersona, cuilPersona, nombrePersona, apellidoPersona, OIDDomicilio, OIDGenero, OIDEstadoCivil, bajaPersona) VALUES (?,?,?,?,?,?,?,?,?,?,0)";
  const INSERTAR_ESTADOCIVIL = "insert into estadocivil (nombreEstadoCivil,bajaEstadoCivil) values(?,0)";
  const INSERTAR_GENERO = "insert into genero (nombreGenero,bajaGenero) values(?,0)";
  const INSERTAR_PAIS = "insert into pais (nombrePais,bajaPais) values(?,0)";
  const INSERTAR_LOCALIDAD = "insert into localidad (nombreLocalidad,OIDProvincia,bajaLocalidad) values(?,?,0)";
  const INSERTAR_PROVINCIA = "insert into provincia (nombreProvincia,OIDPais,bajaProvincia) values(?,?,0)";

  const ACTUALIZAR_PERSONA = "UPDATE persona SET dniPersona = ?, cuilPersona = ?, nombrePersona = ?, apellidoPersona = ?, OIDDomicilio = ?, OIDGenero = ?, OIDEstadoCivil = ? WHERE OIDPersona = ?";
  const ACTUALIZAR_ESTADO_CIVIL = "update estadocivil set bajaEstadoCivil = ?,nombreEstadoCivil = ? where OIDEstadoCivil = ?";
  const ACTUALIZAR_GENERO = "update genero set bajaGenero = ?, nombreGenero = ? where OIDGenero = ?";
  const ACTUALIZAR_PAIS = "update pais set bajaPais = ?, nombrePais = ? where OIDPais = ?";  
  const ACTUALIZAR_PROVINCIA = "update provincia set bajaProvincia = ?, nombreProvincia = ?, OIDPais = ? where OIDProvincia = ?";  
  const ACTUALIZAR_LOCALIDAD = "update localidad set bajaLocalidad = ?, nombreLocalidad = ?,OIDProvincia = ? where OIDLocalidad = ?";    
  
  const BORRAR_PERSONA = "DELETE from persona WHERE OIDPersona = ?";
  
  const BUSCAR_PERSONA_BY_CUIL = "SELECT count(*) FROM persona WHERE cuilPersona = ?";
  const BUSCAR_ESTADO_CIVIL_BY_NOMBRE = "SELECT count(*) FROM estadocivil WHERE nombreEstadoCivil = ?";
  const BUSCAR_GENERO_BY_NOMBRE = "SELECT count(*) FROM genero WHERE nombreGenero = ?";
  const BUSCAR_PAIS_BY_NOMBRE = "SELECT count(*) FROM pais WHERE nombrePais = ?";
  const BUSCAR_LOCALIDAD_BY_NOMBRE = "SELECT count(*) FROM localidad WHERE nombreLocalidad = ?";
  const BUSCAR_PROVINCIA_BY_NOMBRE = "SELECT count(*) FROM provincia WHERE nombreProvincia = ?";

  const BUSCAR_ESTADO_CIVIL_BY_NOMBREID = "SELECT count(*) FROM estadocivil WHERE nombreEstadoCivil = ? and OIDEstadoCivil <> ?";
  const BUSCAR_GENERO_BY_NOMBREID = "SELECT count(*) FROM genero WHERE nombreGenero = ? and OIDGenero <> ?";
  const BUSCAR_PAIS_BY_NOMBREID = "SELECT count(*) FROM pais WHERE nombrePais = ? and OIDPais <> ?";
  const BUSCAR_LOCALIDAD_BY_NOMBREID = "SELECT count(*) FROM localidad WHERE nombreLocalidad = ? and OIDLocalidad <> ?";
  const BUSCAR_PROVINCIA_BY_NOMBREID = "SELECT count(*) FROM provincia WHERE nombreProvincia = ? and OIDProvincia <> ?";

  const BUSCAR_ESTADO_CIVIL_BY_ID = "select * from estadocivil where OIDEstadoCivil = ?";
  const BUSCAR_GENERO_BY_ID = "select * from genero where OIDGenero = ?";
  const BUSCAR_PAIS_BY_ID = "select * from pais where OIDPais = ?";
  const BUSCAR_LOCALIDAD_BY_ID = "select * from localidad l inner join provincia pro on pro.OIDProvincia = l.OIDProvincia inner join pais pa on pa.OIDPais = pro.OIDPais where l.OIDLocalidad = ?";
  const BUSCAR_PROVINCIA_BY_ID = "select * from provincia pro inner join pais pa on pa.OIDPais = pro.OIDPais where pro.OIDProvincia = ?";

  const BUSCAR_ESTADOCIVIL_HABILITADO = "select * from estadocivil where bajaEstadoCivil = 0";
  const BUSCAR_GENERO_HABILITADO = "select * from genero where bajaGenero = 0";
  const BUSCAR_PAIS_HABILITADO = "SELECT * FROM pais WHERE bajaPais = 0";
  const BUSCAR_PROVINCIA_HABILITADO = "SELECT * FROM provincia WHERE bajaProvincia = 0 and OIDPais = ?";
  const BUSCAR_LOCALIDAD_HABILITADO = "SELECT * FROM localidad WHERE bajaLocalidad = 0 and OIDProvincia = ?";
  
  const BUSCAR_PERSONA = "SELECT * FROM persona WHERE OIDPersona = ? AND bajaPersona = 0";
  const BUSCAR_ESTADOCIVIL = "SELECT * FROM estadocivil";
  const BUSCAR_GENERO = "SELECT * FROM genero";
  const BUSCAR_PAIS = "SELECT * FROM pais";
  const BUSCAR_PROVINCIA = "SELECT * FROM provincia pro inner join pais pa on pa.OIDPais = pro.OIDPais";
  const BUSCAR_LOCALIDAD = "SELECT * FROM localidad l inner join provincia pro on pro.OIDProvincia = l.OIDProvincia inner join pais pa on pa.OIDPais = pro.OIDPais";
  
  const BUSCAR_ULTIMO_ID_DOMICILIO = "SELECT max(OIDDomicilio) FROM domicilio";
  const BUSCAR_ULTIMO_ID_CONTACTO = "SELECT max(OIDContacto) FROM contacto";
}

