<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorLocalidad extends ControladorGeneral implements SentenciasDb {
    public function get() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_LOCALIDAD);

        $array = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $array;
    }

     public function getHabilitados() {
        $id = $_GET['habilitado'];
            
        $parametros = array($id);
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_LOCALIDAD_HABILITADO,$parametros);

        $array = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $array;
    }

    public function post() {
        $data = json_decode(file_get_contents('php://input'), true);
    
        $parametrosLocalidad = array($data['nombreLocalidad']);
        $statementNombreLocalidad = $this->ejecutarSentencia(SentenciasDb::BUSCAR_LOCALIDAD_BY_NOMBRE,$parametrosLocalidad);
        $resultadoLocalidad = $statementNombreLocalidad->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoLocalidad['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe una localidad con el nombre ingresado";
        }
        else{
            $parametros = array($data['nombreLocalidad'],$data['OIDProvincia']);    
            $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_LOCALIDAD, $parametros);
            $id = $this->getUltimoId();
            return $this->getById($id);
        }
    }
    public function put() {
        $id = $_GET['id'];
        $data = json_decode(file_get_contents('php://input'), true);
        
        $parametrosLocalidad = array($data['nombreLocalidad'],$id);
        $statementNombreLocalidad = $this->ejecutarSentencia(SentenciasDb::BUSCAR_LOCALIDAD_BY_NOMBREID,$parametrosLocalidad);
        $resultadoLocalidad = $statementNombreLocalidad->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoLocalidad['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe una localidad con el nombre ingresado";
        }
        else{
            $parametros = array($data['bajaLocalidad'],$data['nombreLocalidad'],$data['OIDProvincia'], $id);

            $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_LOCALIDAD, $parametros);

            return $this->getById($id);
        }
    }
    public function getById($id) {
        if(!isset($id)) {
          $id = $_GET['id'];
        }

        if(!$id) {
          return new ApiError("ID inválido");
        }

        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_LOCALIDAD_BY_ID, array($id));
        $arrayPais = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$arrayPais) {
          return new ApiError("No se encontró la localidad");
        }
        return $arrayPais;
    }
}