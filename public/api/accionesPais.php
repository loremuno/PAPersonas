<?php

require_once './llamarControlador.php';
require './ControladorPais.php';

$controladorPais = new ControladorPais();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorPais);

echo json_encode($resultado);
