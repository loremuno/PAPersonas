<?php

require_once './llamarControlador.php';
require './ControladorGenero.php';

$controladorGenero = new ControladorGenero();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorGenero);

echo json_encode($resultado);
