<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorPais extends ControladorGeneral implements SentenciasDb {
    
    public function get() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PAIS);

        $arrayPais = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $arrayPais;
    }

     public function getHabilitados() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PAIS_HABILITADO);

        $arrayPais = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $arrayPais;
    }

    public function post() {
        $data = json_decode(file_get_contents('php://input'), true);
    
        $parametrosPais = array($data['nombrePais']);
        $statementNombrePais = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PAIS_BY_NOMBRE,$parametrosPais);
        $resultadoPais = $statementNombrePais->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoPais['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe un pais con el nombre ingresado";
        }
        else{
            $parametros = array($data['nombrePais']);    
            print_r($parametros);
            $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_PAIS, $parametros);
            $id = $this->getUltimoId();
            return $this->getById($id);
        }
    }
    public function put() {
        $id = $_GET['id'];
        $data = json_decode(file_get_contents('php://input'), true);

        $parametrosPais = array($data['nombrePais'],$id);
        $statementNombrePais = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PAIS_BY_NOMBREID,$parametrosPais);
        $resultadoPais = $statementNombrePais->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoPais['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe un pais con el nombre ingresado";
        }
        else{
            $parametros = array($data['bajaPais'],$data['nombrePais'], $id);

            $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_PAIS, $parametros);

            return $this->getById($id);
        }
    }
    public function getById($id) {
        if(!isset($id)) {
          $id = $_GET['id'];
        }

        if(!$id) {
          return new ApiError("ID inválido");
        }

        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PAIS_BY_ID, array($id));
        $arrayPais = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$arrayPais) {
          return new ApiError("No se encontró el pais");
        }
        return $arrayPais;
    }
}
