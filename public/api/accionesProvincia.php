<?php

require_once './llamarControlador.php';
require './ControladorProvincia.php';

$controladorProvincia = new ControladorProvincia();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorProvincia);

echo json_encode($resultado);
