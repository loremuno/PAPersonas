<?php

require_once './llamarControlador.php';
require './ControladorLocalidad.php';

$controladorLocalidad = new ControladorLocalidad();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorLocalidad);

echo json_encode($resultado);
