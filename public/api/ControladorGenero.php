<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorGenero extends ControladorGeneral implements SentenciasDb {

    public function get() {
        
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_GENERO);

        $arrayGenero = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $arrayGenero;
    }
    
    public function getHabilitados() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_GENERO_HABILITADO);

        $arrayGenero = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $arrayGenero;
    }

    public function post() {
        $data = json_decode(file_get_contents('php://input'), true);
    
        $parametrosGenero = array($data['nombreGenero']);
        $statementNombreGenero = $this->ejecutarSentencia(SentenciasDb::BUSCAR_GENERO_BY_NOMBRE,$parametrosGenero);
        $resultadoGenero = $statementNombreGenero->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoGenero['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe un genero con el nombre ingresado";
        }
        else{
            $parametros = array($data['nombreGenero']);    
            print_r($parametros);
            $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_GENERO, $parametros);
            $id = $this->getUltimoId();
            return $this->getById($id);
        }
    }
    public function put() {
        $id = $_GET['id'];
        $data = json_decode(file_get_contents('php://input'), true);

        $parametrosGenero = array($data['nombreGenero'],$id);
        $statementNombreGenero = $this->ejecutarSentencia(SentenciasDb::BUSCAR_GENERO_BY_NOMBREID,$parametrosGenero);
        $resultadoGenero = $statementNombreGenero->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoGenero['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe un genero con el nombre ingresado";
        }
        else{
            $parametros = array($data['bajaGenero'],$data['nombreGenero'], $id);
            $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_GENERO, $parametros);
            return $this->getById($id);
        }
    }
    public function getById($id) {
        if(!isset($id)) {
          $id = $_GET['id'];
        }

        if(!$id) {
          return new ApiError("ID inválido");
        }

        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_GENERO_BY_ID, array($id));
        $persona = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$persona) {
          return new ApiError("No se encontró el genero");
        }
        return $persona;
    }
}
