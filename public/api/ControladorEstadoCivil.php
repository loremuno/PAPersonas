<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorEstadoCivil extends ControladorGeneral implements SentenciasDb {

    public function get() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ESTADOCIVIL);

        $arrayEstadoCivil = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $arrayEstadoCivil;
    }

     public function getHabilitados() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ESTADOCIVIL_HABILITADO);

        $arrayEstadoCivil = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $arrayEstadoCivil;
    }

    public function post() {
        $data = json_decode(file_get_contents('php://input'), true);
    
        $parametrosEstadoCivil = array($data['nombreEstadoCivil']);
        $statementNombreEstadoCivil = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ESTADO_CIVIL_BY_NOMBRE,$parametrosEstadoCivil);
        $resultadoEstadoCivil = $statementNombreEstadoCivil->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoEstadoCivil['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe una estado civil con el nombre ingresado";
        }
        else{
            $parametros = array($data['nombreEstadoCivil']);    
            print_r($parametros);
            $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_ESTADOCIVIL, $parametros);
            $id = $this->getUltimoId();
            return $this->getById($id);
        }
    }
    public function put() {
        $id = $_GET['id'];
        $data = json_decode(file_get_contents('php://input'), true);

        $parametrosEstadoCivil = array($data['nombreEstadoCivil'],$id);
        $statementNombreEstadoCivil = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ESTADO_CIVIL_BY_NOMBREID,$parametrosEstadoCivil);
        $resultadoEstadoCivil = $statementNombreEstadoCivil->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoEstadoCivil['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe un estado civil con el nombre ingresado";
        }
        else{
            $parametros = array($data['bajaEstadoCivil'],$data['nombreEstadoCivil'], $id);
            $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_ESTADO_CIVIL, $parametros);
            return $this->getById($id);     
        }
    }
    public function getById($id) {
        if(!isset($id)) {
          $id = $_GET['id'];
        }

        if(!$id) {
          return new ApiError("ID inválido");
        }

        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ESTADO_CIVIL_BY_ID, array($id));
        $persona = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$persona) {
          return new ApiError("No se encontró el genero");
        }
        return $persona;
    }
    
}
