<?php

require_once './llamarControlador.php';
require './ControladorEstadoCivil.php';

$controladorEstadoCivil = new ControladorEstadoCivil();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorEstadoCivil);

echo json_encode($resultado);
