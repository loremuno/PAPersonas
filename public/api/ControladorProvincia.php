<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorProvincia extends ControladorGeneral implements SentenciasDb {
    public function get() {
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PROVINCIA);

        $array = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $array;
    }

    public function getHabilitados() {
        $id = $_GET['habilitado'];
            
        $parametros = array($id);
        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PROVINCIA_HABILITADO,$parametros);

        $array = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $array;
    }

    public function post() {
        $data = json_decode(file_get_contents('php://input'), true);
    
        $parametrosProvincia = array($data['nombreProvincia']);
        $statementNombreProvincia = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PROVINCIA_BY_NOMBRE,$parametrosProvincia);
        $resultadoProvincia = $statementNombreProvincia->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoProvincia['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe una provincia con el nombre ingresado";
        }
        else{
            $parametros = array($data['nombreProvincia'],$data['OIDPais']);    
            $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_PROVINCIA, $parametros);
            $id = $this->getUltimoId();
            return $this->getById($id);
        }
    }
    public function put() {
        $id = $_GET['id'];
        $data = json_decode(file_get_contents('php://input'), true);

        $parametrosProvincia = array($data['nombreProvincia'],$id);
        $statementNombreProvincia = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PROVINCIA_BY_NOMBREID,$parametrosProvincia);
        $resultadoProvincia = $statementNombreProvincia->fetch(PDO::FETCH_ASSOC);
        $contador = $resultadoProvincia['count(*)'];
   
        if($contador==1){
            header('X-PHP-Response-Code: 500', true, 500);
            return "Ya existe una provincia con el nombre ingresado";
        }
        else{
            $parametros = array($data['bajaProvincia'],$data['nombreProvincia'],$data['OIDPais'], $id);

            $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_PROVINCIA, $parametros);

            return $this->getById($id);
        }
    }
    public function getById($id) {
        if(!isset($id)) {
          $id = $_GET['id'];
        }

        if(!$id) {
          return new ApiError("ID inválido");
        }

        $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PROVINCIA_BY_ID, array($id));
        $array = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$array) {
          return new ApiError("No se encontró la provincia");
        }
        return $array;
    }
}