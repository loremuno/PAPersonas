<?php

function llamarControlador($metodo, $controlador) {
  switch($metodo) {
    case 'GET':
      if(isset($_GET['id']))
        return $controlador->getById($_GET['id']);
      if(isset($_GET['habilitado'])) 
        return $controlador->getHabilitados();
      else
        return $controlador->get();  
    case 'POST':
      return $controlador->post();
    case 'PUT':
      return $controlador->put();
    case 'DELETE':
      return $controlador->delete();
    default:
      return 'Método inválido';
  }
}
