<?php
require './ControladorGeneral.php';
require './SentenciasDb.php';
require './ControladorHttp.php';
require './ApiError.php';

/**
 * Description of ControladorPersonas
 *
 * @author ieltxu
 */
class ControladorPersonas extends ControladorGeneral implements SentenciasDb, ControladorHttp {

  public function get() {
    $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PERSONAS);
    $arrayPersonas = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    return $arrayPersonas;
  }

  public function getById($id) {
    if(!isset($id)) {
      $id = $_GET['id'];
    }

    if(!$id) {
      return new ApiError("ID inválido");
    }

    $statement = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PERSONA, array($id));
    $persona = $statement->fetch(PDO::FETCH_ASSOC);

    if (!$persona) {
      return new ApiError("No se encontró la persona");
    }
    return $persona;
  }

  public function post() {
    $data = json_decode(file_get_contents('php://input'), true);
    
    $parametrosDomicilio = array($data['calleDomicilio'], $data['numeroDomicilio'], $data['pisoDomicilio'], $data['deptoDomicilio'], $data['OIDLocalidad']);
    $statementDomicilio = $this->ejecutarSentencia(SentenciasDb::INSERTAR_DOMICILIO, $parametrosDomicilio);
    $idDomicilio = $this->getUltimoId();
    if($idDomicilio==0){
        $statementOIDDomicilio = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ULTIMO_ID_DOMICILIO);
        $idDomicilio1 = $statementOIDDomicilio->fetch(PDO::FETCH_ASSOC);
        $idDomicilio = $idDomicilio1['max(OIDDomicilio)'];
    }
    
    $parametrosContacto = array($data['celularContacto'], $data['correoElectronicoContacto'], $data['telefonoAlternativoContacto'], $data['telefonoFijoContacto']);
    $statementContacto = $this->ejecutarSentencia(SentenciasDb::INSERTAR_CONTACTO, $parametrosContacto);
    $idContacto = $this->getUltimoId();
    if($idContacto==0){
        $statementOIDContacto = $this->ejecutarSentencia(SentenciasDb::BUSCAR_ULTIMO_ID_CONTACTO);
        $idContacto1 = $statementOIDContacto->fetch(PDO::FETCH_ASSOC);
        $idContacto = $idContacto1['max(OIDContacto)'];
    }
    
    $parametrosPersona = array($data['cuilPersona']);
    $statementPersona = $this->ejecutarSentencia(SentenciasDb::BUSCAR_PERSONA_BY_CUIL, $parametrosPersona);
    $resultadoPersona = $statementPersona->fetch(PDO::FETCH_ASSOC);
    $contadorPersona = $resultadoPersona['count(*)'];
    
    if($contadorPersona==1){
        header('X-PHP-Response-Code: 500', true, 500);
        return "Ya existe una persona con el CUIL ingresado";
    }
    else{
        $parametros = array($idContacto,$data['apellidoMaternoPersona'],$data['fechaNacimientoPersona'],$data['dniPersona'], $data['cuilPersona'], $data['nombrePersona'], $data['apellidoPersona'], $idDomicilio, $data['OIDGenero'], $data['OIDEstadoCivil']);    
        print_r($parametros);
        $statement = $this->ejecutarSentencia(SentenciasDb::INSERTAR_PERSONA, $parametros);
        $id = $this->getUltimoId();
        return $this->getById($id);
    }
  }

  public function put() {
    $id = $_GET['id'];
    $data = json_decode(file_get_contents('php://input'), true);

    $parametros = array($data['dniPersona'], $data['cuilPersona'], $data['nombrePersona'], $data['apellidoPersona'], $data['OIDDomicilio'], $data['OIDGenero'], $data['OIDEstadoCivil'], $id);

    $resultado = $this->ejecutarSentencia(SentenciasDb::ACTUALIZAR_PERSONA, $parametros);

    return $this->getById($id);
  }

  public function delete() {
    $id = $_GET['id'];

    $parametros = array($id);

    return $this->ejecutarSentencia(SentenciasDb::BORRAR_PERSONA,$parametros);
  }
}
