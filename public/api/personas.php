<?php

require_once './llamarControlador.php';
require './ControladorPersonas.php';

$controladorPersonas = new ControladorPersonas();
$resultado = llamarControlador($_SERVER['REQUEST_METHOD'], $controladorPersonas);

echo json_encode($resultado);
